#!/bin/bash
#Wrapper script for metagenomic wordkflow in Docker
#Samuel Gerner, samuel.gerner@fh-campuswien.ac.at, 170412

##########################################################################
### Subroutines and example data #########################################
##########################################################################

########################
#default configfile creation
configfile() {
echo -e "#################################################################
### Configfile for Metagenomics Taxonomy Workflow, created: $DATE
#################################################################
# To disable a parameter, use value 'False' (preferred over deletion of line)
# CAVEAT: Keep order of arguments, especially for tools: Trimmomatic, CheckFASTQ, Decontaminate
#################################################################
### Parameters #########
########################
Cutadapt:minlength		200
Cutadapt:maxlength		600
Cutadapt:minqual		20
#CAVEAT Trimmomatic options will be performed in the exact order as in the configfile, e.g. minlen should be last
Trimmomatic:jarfile		/opt/Trimmomatic-0.36/trimmomatic-0.36.jar
Trimmomatic:leading		10
Trimmomatic:trailing		10
Trimmomatic:slidingwindow	4:15
Trimmomatic:minlen		70
Repeatmasker:threshold		50
Qiime:revStrandMatch		True
Kraken:database			/opt/databases/minikraken_20141208
Krona:db			/opt/databases/Krona_db/
ChimeraCheck:database		/opt/databases/SILVA_128_QIIME_release/rep_set/rep_set_16S_only/97/97_otus_16S.fasta
MetaPhlan2:dbfiles		/opt/metaphlan2/db_v20/mpa_v20_m200
Decontaminate:program		snap-aligner
Decontaminate:folder		/opt/databases/Decontaminate_references
Humann2:db			/opt/databases/humann2
Humann2:memory			maximum
##########################################################################
### Values for not self-explanatory Parameters ###########################
##########################################################################
#Repeatmasker:threshold		Discard sequences with less than the minimum percentage specified of unmasked residues
#MetaPhlan2:dbfiles		this setting is needed for db and bowtie2 files - please set parameter without ending (.pkl , .*.bt2)
#Decontaminate:program		bowtie2|snap-aligner(=default), choose bowtie2 for saving disk space or if a pre-build bowtie2 index is already available (running bowtie2 is about equally fast as building index + running for snap)
#Decontaminate:folder		path to the fasta reference file(s) of the species to be excluded (e.g. from human genome)
#Humann2:memory-use 		minimum|maximum
##########################################################################
### Mandatory flags for Dependencies #####################################
##########################################################################
#Trimmomatic:jarfile		/path/to/trimmomatic-x.xx.jar
#Kraken:database		/path/to/kraken-db e.g. '/path/to/minikraken_20141208/'
#ChimeraQiime:database		/path/to/reference-db e.g. '/path/to/rdp_gold.fa'
#MetaPhlan2:dbfiles		/path/to/reference-db e.g. '/path/to/db_v20/mpa_v20_m200'
#Humann2:db			/path/to/reference-db e.g. '/path/to/humann2'
#Decontaminate:folder		/path/to/fasta file / index file-folder e.g. '/path/to/chicken_index/'" > $OUTDIR/configfile
}

########################
#help message
usage() { echo -e "Usage: $0 -a <reads_f.fq.gz> [-b <reads_r.fq.gz>] -o <Outputdir> -c <Configfile> -m <mode> [other Options]\n
\t-a\tForward Fastq/Fasta File
\t-b\tReverse Fastq/Fasta File (paired end assumed if provided)
\t-o\tOutput Directory (default current working directory)
\t-m\tMode (wgs|16S)
\t-c\tConfigfile (either provide custom configfile or use 'default')
\t-t\tThreads (default 1)
\t-d\t[OPTIONAL] Remove host DNA
\t\tif set 'true' parameter 'Decontaminate:folder' must be set in config file
\t-k\t[OPTIONAL] Don't remove temporary folder with all the intermediated files [default:false])
\t-x\t[OPTIONAL] Only do taxonomic and no functional analysis
\t-r\tOnly write out default configfile to current directory and exit
\n\t-h\tPrints this help message\n
>>>>> Info: For naming Output, filenames like 'readname_orientation.format' are expected <<<<<\n
Example:
\t$0 -o /path/to/outdir -c default -a /path/to/reads_f.fq.gz -b /path/to/reads_r.fq.gz -m wgs
\t$0 -o /path/to/outdir -c /path/to/configfile -a /path/to/reads_1.fq.gz -b /path/to/reads_2.fq.gz -m wgs
\t$0 -o /path/to/outdir -c default -a /path/to/reads.fa.gz -m 16S
\t$0 -r";
exit; }


##########################################################################
### Checks and Command line parsing ######################################
##########################################################################

########################
#setup starting vars
#save current date with time for logging
DATE=$(date +"%y%m%d_%T")
#save starting point of script for running time
STARTM=$(date -u "+%s")

echo "######################################################################################################"
########################
#parse command line options
while getopts ":a:b:c:o:m:t:rhxk" opt; do
	case $opt in
		a)
		READSF=$OPTARG
		if [[ ! -r $READSF || ! -f  $READSF ]]; then
		>&2 echo -e "ERROR: Forward fastq file not readable\n"; exit 1 ; fi
		;;
		b)
		READSR=$OPTARG
		if [[ ! -r $READSR || ! -f  $READSR ]]; then
		>&2 echo -e "ERROR: Reverse fastq file not readable\n"; exit 1;  fi
		;;
		c)
		if [[ $OPTARG == "default" ]]; then
			DEFAULTCONFIG="1"
		else
			if [[ -r $OPTARG ]]; then
				CONFIGF=$OPTARG
			else
				>&2 echo -e "ERROR: Configfile not readable\n"; exit 1;
			fi
		fi
		;;
		d)
		#TODO decontamination not implemented in docker, as reference file need to be mounted
		DECONTCONFIG="1"
		if [[ $OPTARG == 'human' ]] ; then
			DECONT_REF="/hg19.fa"
		elif [[ $OPTARG == 'chicken' ]] ; then
			DECONT_REF="/gga_ref_Gallus_gallus-5.0_chr28.fa"
		else
			>&2 echo -e "ERROR: Parameter provided for option -d not valid\n"; exit 1;
		fi
		;;
		k)
		TMP_KEEP="1"
		;;
		x)
		TAXONLY="1"
		;;
		t)
		THREADS=$OPTARG
		;;
		o)
		OUTDIR=$OPTARG
		if [[ -d $OUTDIR ]]; then
			echo -e "WARNING: Outputdirectory '$OUTDIR' already exists, results may be overwritten"
		else
			mkdir $OUTDIR
		fi
		;;
		m)
		MODERE='wgs|16S|test'
		if [[ $OPTARG =~ $MODERE ]]; then
			MODE=$OPTARG
		else
			>&2 echo "ERROR: Mode not recognized, please provide valid choice"
			usage
		fi
		;;
		r)
		echo -e "Create Configfile in current working directory and exit\n"
		OUTDIR="./"
		configfile
		exit;
		;;
		h)
		usage
		;;
		\?)
		>&2 echo -e "ERROR: Invalid option: -$OPTARG\n" >&2; usage
		;;
		:)
		>&2 echo -e "ERROR: Option -$OPTARG requires an argument\n" >&2; usage
		;;
	esac
done

########################
#if defaultconfigfile is selected, create default config
if [[ $DEFAULTCONFIG == "1" ]]; then
	configfile
	CONFIGF="$OUTDIR/configfile"
else
        cp $CONFIGF $OUTDIR/configfile
        CONFIGF="$OUTDIR/configfile"
fi

#in case of PE, check if both forward and reverse reads are in the same directory
if [[ $READSF && $READSR ]]; then
	if ! [[ $(dirname $READSF) == $(dirname $READSR) ]]; then
		>&2 echo -e "ERROR, forward and reverse reads not in the same input directory, please input both files from one directory\n"
		exit 1
	fi
fi

DATE=$(date +"_d%y%m%d_t%H%M%S")

########################
#change potential relative dir path into absolute path to enable mounting by docker
ABSOUTDIR=$(cd $OUTDIR;pwd)

ABSREADDIR=$(cd $(dirname $READSF);pwd)

########################
#execute docker

DOCKERCMD="docker run -u $(id -u):$(id -g) -v $ABSREADDIR/:/Input -v $ABSOUTDIR/:/workdata fhcwbioinf/wgs-full-db -c /workdata/configfile -o /workdata/output$DATE -m $MODE -a /Input/$(basename $READSF)"
if [[ $REASDR ]]; then
	DOCKERCMD="$DOCKERCMD -b /Input/$(basename $READSR)"
elif [[ $THREADS ]]; then
	DOCKERCMD="$DOCKERCMD -t $THREADS"
elif [[ $TAXONLY ]]; then
	DOCKERCMD="$DOCKERCMD -x"
elif [[ $TMP_KEEP ]]; then
	DOCKERCMD="$DOCKERCMD -k"
fi
echo "Starting Docker Container..."
eval "$DOCKERCMD"
if ! [[ $? == 0 ]]; then
	>&2 echo "Docker command failed, consider pulling current image via 'docker pull fhcwbioinf/wgs-full-db' after logging  in to docker via 'docker login'"
fi

########################
#clean up
#delete configfile outside of output directoy, as dockercommand writes it's own configfile again
rm $OUTDIR/configfile

#Docker container creates '?' directory originating from Java, remove it
rm -rf $OUTDIR/output$DATE/?
